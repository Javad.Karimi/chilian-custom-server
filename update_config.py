import json
import sys

map_paths = ['maps/AllIn.json', 'maps/Artileryyyy.json', 'maps/NoTime.json', 'maps/PanzerStorm.json',
             'maps/WhoNeedsTank.json']


j = {
    "general": {
        "game_name": "Logistics",
        "command_files": [
            "app/ks/commands"
        ],
        "allowed_teams": {},
        "allow_duplicate_team_nicknames": True,
        "offline_mode": True,
        "": "online configs",
        "match_id": "0"
    },

    "net": {
        "host": "127.0.0.1",
        "port": 5000,
        "keyfile": "/builds/Javad.Karimi/ChillinWars/chilian-custom-server/certs/key.pem",
        "certfile": "/builds/Javad.Karimi/ChillinWars/chilian-custom-server/certs/cert.pem"
    },

    "gui": {
        "host": "127.0.0.1",
        "port": 5001,
        "max_spectators": 3,
        "save_replay": True,
        "replay_dir": "replays",
        "replay_filename": None,
        "cycle_duration": 1.0,
        "side_colors": {
            "Rebellion": "#707828",
            "Regular": "#303859"
        }
    },

    "sides": {
        "Rebellion": ["0"],
        "Regular": ["0"]
    },

    "game_handler": {
        "": "base game handler configs",
        "start_time": "2017-11-08 00:50",
        "start_waiting_time": 1,
        "": "realtime game handler configs",
        "cycle_duration": 0.1,
        "first_cycle_duration": 3,
        "max_total_commands": 200,
        "max_team_commands": 100,
        "max_agent_commands": 100,
        "": "my configs",
        "map_path": "maps/{}".format(sys.argv[1])
    }
}
with open('gamecfg.json', 'w') as f:
    json.dump(j, f)
